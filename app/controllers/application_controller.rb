class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  layout false

  rescue_from Exception,                           with: :render_error
  rescue_from ActiveRecord::RecordNotFound,        with: :render_not_found
  rescue_from ActiveRecord::RecordInvalid,         with: :render_record_invalid
  rescue_from ActionController::RoutingError,      with: :render_not_found
  rescue_from AbstractController::ActionNotFound,  with: :render_not_found
  rescue_from ActionController::ParameterMissing,  with: :render_parameter_missing

  def status
    render json: { online: true }
  end

  private

  def render_error(exception)
    raise exception if Rails.env.test?

    # To properly handle RecordNotFound errors in views
    return render_not_found(exception) if exception.cause.is_a?(ActiveRecord::RecordNotFound)

    return if performed?

    render json: { error: 'Server error' }, status: :internal_server_error
  end

  def render_not_found(_exception)
    render json: { error: 'Not found' }, status: :not_found
  end

  def render_record_invalid(exception)
    render json: { errors: exception.record.errors.as_json }, status: :bad_request
  end
end
