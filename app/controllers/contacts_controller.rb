class ContactsController < ApplicationController
  def index
    @contacts = Contact.all
  end

  def create
    @contact = Contact.create!(contact_params)
  end

  def show
    @contact = ::Contact.find(params[:id])
    return head :not_found unless @contact.present?
  end

  def destroy
    Contact.destroy(params[:id])
  end

  def update
    contact = ::Contact.find(params[:id])
    @contact = contact.update(contact_params)
  end

  private

  def contact_params
    params.require(:contact).permit(:id, :first_name, :last_name, :phone, :email)
  end
end
